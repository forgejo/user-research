# Repository settings survey

In October 2024, we started a survey about the repository settings.
Motivation  is my quest to rework some of the settings.
Directly related design issues include:

- [Unavailable settings: hide / disable](https://codeberg.org/forgejo/design/issues/16)
- [Future of the wiki tab](https://codeberg.org/forgejo/design/issues/3)
- [Add new consistency](https://codeberg.org/forgejo/design/issues/19)

The poll ran from 2024-10-05 to 2024-10-20,
and users were encouraged to participate using a banner in the repository settings
which was displayed on Codeberg.org.

The survey was conducted using Cryptpad.fr as an external service,
mainly because I find that the forms app is powerful yet easy to use.
It is not an optimal choice, though, because their accessibility is limited,
also see https://github.com/cryptpad/cryptpad/issues/1601
which I created on my quest to improve accessibility for a similar element in Forgejo.

## Analysis

I added a column for keywords and notes each,
then went through the entries one by one to take notes.

### Tooling

It was a challenge to find a viewer that allows me to view only one entry at a time,
but showing all of this information simultaneously.
I considered using LibreOffice, making the data span two monitors,
but I found this rather confusing.
Further, I was mostly not at home during the work,
such as in a train or in university, with no access to a big screen.

Finally, I used [visidata](https://www.visidata.org/) to operate the file, opened using:

~~~sh
visidata --csv-lineterminator $'\n' --quitguard surveys/repository-settings/2024-10_codeberg.csv
~~~

Pressing Enter on a line opens it in another view that shows only this single entry
and uses screen space better.
Pressing `v` is the keybinding for multi-line which usually allowed to also read
lines that were truncated.

I lost quite some progress due to frequently quitting inadvertently without saving.
The `--quitguard` option is not the default,
which caught me very bad.

## Conclusions

### Meta (label: `meta`)

Some people were confused by the lack of translation of the banner,
which was only shown in English.

I don't know if it's worth translating such occassional banners,
but I also think that similar confusion happens when languages are not 100% completed.
We should aim for 100% completion rates as much as possible.

A lot of people were confused by the type of my questions.
I think I tried to "emulate" a moderated session too much,
and suggest making future surveys more simple.
