---
name: "Your first hand experience"
about: "Tell us about your experience and help build the Forgejo roadmap"
title: "Your first hand experience"
---
<!--

Forgejo tries to adopt a user centric approach, which starts by trying to figure out why and how a feature helps the user. The method is called [User Research](https://codeberg.org/forgejo/user-research).

If you'd like to help do that, you could file this issue. It is still unusual in the Free Software world but maybe the link above will convince you that it is worth your time. The outcome of this User Research is to sort which features are most important for volunteers and paid staff to work on together. It is a sound method to go from an unsorted, ever growing, list of feature request to a user centric roadmap where what matters most to the user (and not the paying customer or the manager) gets done first.

It is possible that a User Research report has been written by someone else about that specific feature and contains all the material Forgejo would need. Unfortunately it is almost never publicly available and there is a need to start from scratch. It also possible (and quite common) that this feature was just added for no other reason than a manager or a client paying for it. Which is exactly what Forgejo is trying to avoid.

-->

# About you

- What Free Software projects did you contribute to? Please provide URLs that show what you have done by yourself.
  1. https://...
  1. https://...
- What are your areas of expertise (use `[x]`)?
  - [ ] User Interface
  - [ ] User eXperience
  - [ ] Web design
  - [ ] Infrastructure
  - [ ] Sysadmin
  - [ ] Development (whatever the programming language)
  - [ ] Community management
  - [ ] Funding
  - [ ] Mentoring
  - [ ] Project management

# Your first hand experience

<!--

Explain, in chronological order, step by step, what you did and how. Inlude screenshots if necessary.

Here is an example:

I created a bug report at https://codeberg.org/forgejo/forgejo

* I visited https://codeberg.org/forgejo
* I noticed https://codeberg.org/forgejo/forgejo because it had a logo
* I clicked on the link and saw https://codeberg.org/forgejo/forgejo
* I clicked on "Issues" and saw https://codeberg.org/forgejo/forgejo/issues
* I typed "pipeline" in the **Search** input box and pressed enter
* The results show a few issues although none of them had pipeline in the title which led me to the conclusion that the search was also on the issue description
* None of the issue titles was a match for the problem I was facing
* I clicked on **New Issue**
* The first choice was **Bug Report** and I clicked on the **Get Started** button
* etc.

-->

* I ...
* ...
