# Forgejo User Research

The purpose of this repository is to store raw notes on user behaviour and observations
which can be used by Forgejo developers to make decisions in their work
and especially in the [Forgejo design process](https://codeberg.org/forgejo/design/issues).

Thanks to everyone for making user research a thing in Forgejo,
and especially everyone who took the time to participate in our interviews so far.


## What is in the box?

- notes from moderated sessions / interviews:
	- user testing (observing people while using Forgejo and taking notes about potential problems and confusion they encounter)
	- user reserach (interviewing users about their usage or observing them in an environment (not necessarily Forgejo), usually with the goal to learn about their needs)
- proactive feedback from contributors
- other raw data that is collected from all over the place for later reference

The data is clustered and sorted in an "effort-efficient" way:
The goal is to retrieve related information when needed,
but not to spend too much time drawing conclusions here
(This is currently happening in the design repo instead).

Sometimes, we do interview sessions, sometimes with a specific focus.
These interviews are grouped together with a file providing more context.
Examples include:

* [June 2024 user testing](interviews/2024-06)
* [April 2024 user testing](interviews/2024-04)


## What do we want to learn?

User research is about learning more. For example about:

- our users: What do they need to work efficiently with Forgejo? does Forgejo allow everyone to participate in software development (e.g. designers, translators etc)?
- Forgejo admins: Which challenges do they run when hosting Forgejo? How do they deploy Forgejo?
- our contributors: How easy was it to get started? Did they encounter any challenges, technical or social? Can Forgejo do better in encouraging diverse contributions?
- indirect users (people who didn't decide to use Forgejo but interact with them due to their interaction with other Forgjeo users,
  e.g. end-users downloading a release from Forgejo or reporting an issue to the developers):
  Do they find Forgejo intuitive and easy to use? Can they perform their desired task efficiently without a background on similar platforms?

Sometimes, we want to learn about specific things that aid with specific design considerations
(such as learning about how people use the issue tracker and how we can improve the sorting of the sidebar to maximize their efficiency).

But at the same time, user research can offer surprising and interesting insights that we would have not thought about otherwise.
This kind of input is very important to us and it is hard to obtain via other means (like feature requests or surveys).
Every session provides some unique value.


## Contributing to user research

User reserach can be a little "tough" to get started with,
not because it is "hard" but because you need research subjects and a little preparation.
I recommend to read some material (see below) and reach out if you are interested to help.
We're not experts either, just getting started as well!

We invite you to reach out and/or to assist in taking notes during public Forgejo events.
Or, if you have people around you (physically or in online communities),
ask them to use Forgejo while you spectate, take some notes and publish them here with their consent.
This can allow you to get used to the workflow.


## Learning about user reserach

Our user reserach is inspired by some valuable resources. We recommend taking a look at:

- Must Read: ["A Beg­inner’s Guide to Finding User Needs"](https://jdittrich.github.io/userNeedResearchBook/)
- the [Open Source Design community](https://discourse.opensourcedesign.net/)
- ["First Rule of Usability? Don't Listen to Users"](https://www.nngroup.com/articles/first-rule-of-usability-dont-listen-to-users/)


## Raw data

Primary source of data related to an activity.

### Contributor

* Writing Forgejo release notes - Pull requests [updating the RELEASE-NOTES.md](https://codeberg.org/forgejo/forgejo/commits/branch/forgejo/RELEASE-NOTES.md).
* Publishing Forgejo releases - The checklist of [each Forgejo release](https://codeberg.org/forgejo/forgejo/milestones?sort=furthestduedate), [for instance for Forgejo v1.21.11-2](https://codeberg.org/forgejo/forgejo/issues/4128). It is easily found because the title of the issue starts with a flame.
