In [Forgejo](https://www.forgejo.org), we are looking for people who would be willing to answer a one hour interview related to their experience working with issues on a forge and how the lack of accessibility made it more difficult to participate. All the interviews will be conducted according to our [code of conduct](https://forgejo.org/code-of-conduct/).

If you are that person please communicate opening a conversation in [our account in Mastodon](https://floss.social/@forgejo), using the hashtag \#ForgejoAccessibility or opening an issue in our [User research repository](https://codeberg.org/forgejo/user-research/issues).

If you feel comfortable with it, the interview  will be published as part of a study case in [the user research repository of Forgejo](https://codeberg.org/forgejo/user-research) in order to build a roadmap to improve accessibility in our software forge and to provide that data for the public benefit.
