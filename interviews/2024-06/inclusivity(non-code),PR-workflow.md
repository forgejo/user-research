**Opening the session:**

- background and motivation:
	- past open source contributor, became involved due to a past contact
	- has free time, is contributing since April and wants to get more involved
	- main role: non-code contributor
	- is confused about the technical language, esp. the pull request / fork workflow, and has difficulty with the documentation
	- would have appreciated a **search feature in the docs**
	- estimate time using Forgejo: often 15-20hrs/week, sometimes less
- ask: last thing they did using Forgejo: write non-code documents using the Forgejo Git web UI


**Move files in the web editor**

- status: PR is open
- task: find the path where to edit the file again
	- visits target branch in main repo (file view)
	- visits branches in target repo
	- updates URL manually to find the repo
	- preses the fork button to find their work
	- expects their ongoing contribution to be listed (but it isn't, because it is not on the main branch)
	- visits branches, finds the notes
- task: move the files to a subfolder
	- tries add file
	- tries the dropdown
	- tries the "add more" button and is lead to the repo settings
- do: help and explain folders
	- > "in edit mode, you need to remember the exact location name"
	- UI is a little flaky because it suddenly alters the text field to represeent the correct location
- copies the path from the web ui and inserts it
- uses back button in browser to find the (now renamed) file, resulted in 404
- general confusion about PR and their direction
- confusion about the "This branch is out of date, update branch" information
