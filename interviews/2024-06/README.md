# User Interviews Take Two

The second round of Forgejo user interviews:
A little smaller, only a handful of people and a few days (June 26 - 28, 2024) only.


## Methodology

- Nextcloud Calendar for booking appointments
- several reserved timeslots, 45 minutes per interview
- manual invite links for BigBlueButton and OpenTalk conferences
	- OpenTalk seems to have much better UX, so we'll likely stick to it in the future
- nothing was recorded, but I copied notes into textfiles before each meeting
- there were basically two kinds of sessions:
	- people start to talk right away or demonstrate their problems, I listen
	- people used Codeberg or their Forgejo instance of choice to complete tasks I gave them


## Diversity

Due to much smaller sample size than the previous round,
less different groups of Forgejo were covered.


## General Impressions

Some things I want to share, because I think they generally help in making Forgejo a better product.

Verified: "Persistent filters" from 2024-04 research.

**Flexible Dashboard.**
Ideas for a new dashboard was always a topic for me during this and the past user research.
However, I noticed that there is not one user need.
The need is for flexibility,
and users already bookmark various important pages.
Examples include: Checking the admin's runner menu to see latest job runs and their status,
or bookmarking certain PR tabs and filters to see what automation did recently.
Whatever we do, we likely want to make it highly configurable.

**Federation.**
People are waiting for it.
The promise of having federation one day is often motivation enough to use Forgejo today.
Federation has numerous challenges,
but shifting focus on doing this properly,
and potentially shipping first features soon is what users *want*.
Implementing other means of external collaboration might also address many users' needs.


## Conclusions

I'm personally much less happy with this round of user testing.
The past one was the "new experiment".
I was excited and nervous all the time.
This one felt much more like actual work and was less fun.

I didn't get the results that I wanted to get.
In retrospect, this was not much different from the research session before:
Only a handful sessions answered the questions I had,
most of them gave otherwise valuable input.
In the end, I had the answers I wanted and additional input.

The sessions in this round were not "worse",
but due to the smaller sample size,
I got only a few questions answered,
and additional insights.
They are valuable, too,
but the open answers make me feel much less successful.

The current effort/gain ratio with regards to answering questions
and actually turning them into an implementation is not very good.

I believe we need to make user testing more efficient,
either by running larger campaigns again (with more questions and better potential matching to the users),
or by using technical means to simplify and automate the workflow.

I hoped to regularly repeat such "smaller" rounds,
but I think this does not work out on the long-term.

Looking forward to turning some of the feedback into actual improvements for Forgejo, though!
Thank you for reading.

~ fnetx

