General questions to keep in mind during the session:

- Are there generic obstacles when navigating the page?
- Are there special moments of delay, e.g. caused by slow page loads, confusion or otherwise unexpected behaviour?
- When navigating the page, are there **frequent / repeated** actions that are **placed far apart**
  (e.g. visually or tab order on keyboard navigation?)


**Opening the session:**

- welcome, thanks, state the goal
- no audio/video/screen recording, but anonymized notes
- background and motivation:
	- main role: developer | maintainer | manager | translator | enduser | designer | something else?
	- estimate time using Forgejo: 
	- idea about projects:
- screenshare:
	- whole screen (for the visibility of context menues)
	- recommend disabling notifications and entering fullscreen mode (prevent information leak)
- ask: last thing they did using Forgejo: 
- encourage "thinking aloud"
	- and sometimes remind them about it, for example by asking "What are you thinking right now?"
- ask: typical **first action of the day**
	- e.g. checking notifications, visit dashboard
	- let them perform their routine if it matches one of the tasks below or doesn't take significant time


**Dashboard/Notifications**

- do: perform first daily action
	- use of notifications or dashboard?
- freestyle ...


**Pronouns**

- Ask: Aware of pronouns / respect?
- ask: set pronouns yourself?
- let: specify an option
- let: interact with a user and obtaining the pronouns


**Issues**

- ask: determine if strong user of issues
- ask: interaction with issue sidebar?
- let: perform various actions
- ask: label strategies
- ask: projects?
- let: reference one issue in another by mentioning it


**Admins**

- ask: deployment / setup
- ask: users? which (friends vs public etc)
- ask: moderation?
- let: open own admin settings
- ...


**PR / Reviewer**

- let: clone one PR locally
- let: (if complicated) there is a command / instructions that you can copy-paste
	- where do they look?
- let: review normally


**Actions**

..


**Code Search**

- let: find usage of ContextUser in Forgejo
- let: check unused strings
- let: other use cases?


**New feature compilation**

- let: simultaneous issue comment edit
- let: edit a code file in new editor (the new tab behaviour etc) https://codeberg.org/forgejo/forgejo/pulls/4072
