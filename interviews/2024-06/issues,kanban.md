**Opening the session:**

- background and motivation:
	- own instance and Codeberg
	- estimate time using Forgejo: 5-6 hrs/week
	- idea about projects: small projects (own instance), static websites
- ask: last thing they did using Forgejo: struggling with Forgejo Actions


**Issues**

- 400 issues
- mostly labels and projects
- **bookmarks** with typical keyword / label combinations (verifies "Persistent Filters" from 2024-04 user research)
- never used assign to branch / tag
- copy-pastes link for cross-reference
- issue templates a lot
	- cannot **predefine project**
	- no assignee
- kanban board: automatic label assignment per list (was a blocker for agile workflow)
	- works in GitLab in both directions
	- alternative: filter project column in issue list
	- doesn't use Forgejo at work due to this shortcoming


**Admins**

- likes Forgejo being lightweight


**Actions**

- from Woodpecker to Actions: simplicity for own instance instead of hosting woodpecker
