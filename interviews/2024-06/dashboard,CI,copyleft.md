**Opening the session:**

- background and motivation:
	- selfhosted in home lab
	- coach, tries to understand Forgejo and explain it to other people
	- enable small companies etc to use Forgejo instead of surveillance tech
	- main role: devops
	- estimate time using Forgejo: every day
	- idea about projects: devops scripts, model infrastructure for demonstration purposes
- ask: last thing they did using Forgejo: tried to set **avatar, was some few pixels too large**
- ask: typical **first action of the day**
	- check pull requests, created by automation (renovate)
	- 4.5k PRs on own instance
	- check if **build steps failed for whatever reasons** (dashboard)


**Admins**

- SSO authentik


**Actions**

- home lab automation etc
- learning a lof of devops
- would like to share actions (config) with the world, but **hide actual runs / logs**
	- (e.g. to prevent secret leaking or otherwise leaking details)
	- hidden workflows?
- recently implemented (was looking for it): skip ci
- **branch protection** difficult with dynamic branches
- ask: any blockers or problems?
- attestation of artefacts during deployment
- pull in additional properties for users?
- e.g. SSH key from LDAP per user
- currently, SSH authentication on the home network makes the ssh-agent open a browser window for SSO to obtain the certificate (SSH Principal)
- this is not possible in Actions workflows, obviously
- bookmarks their single runner for actions logs (candiate for the **dasbhoard**?)
	- user appears to have high need for a highly customizable dashboard


**Copyleft**

- ask: problem for enterprise customer?
- AGPL is a bad keyword for some, but not everywhere
