**Opening the session:**

- background and motivation:
	- main role: developer
	- estimate time using Forgejo: daily
	- Codeberg user
	- idea about projects: regularly maintained projects, membership tracking app
	- came from GitHub


**General**

- user tried to add styles to elements in README (border-radius), surprised they got stripped, GitHub strips it, too
- "terrible discovery features"
	- do: explain the new ideas
	- user suggests a search bar in the navbar


**Pronouns**

- user already filled pronouns
- ask: importance people respect pronouns: very important
- ask: looks up pronouns for other users? yes


**Issues**

- not enough issues to work with labels
- some auto-tagged by the template or tagged by team members
- ask: how many team members? 5 people
- users "closes: #x " in PRs
- uses issue search


**PR / Reviewer**

- let: review one PR
- let: clone one PR locally
	- searches top buttons in files view (download patch etc)
	- searches commits view next
	- copies branch name from main view
	- clones and checks out the branch
	- imported from GitHub
- main code view: **clone URL should update when on different branch**
- "create squash commit" confusing: sounds like creating new commit instead of combining the existing
	- suggestion: "Squash and Merge" ?
- uses squash commits
- user missed **"delete branch" checkbox**, wants this as a default


**Actions**

- pushing packages (to Codeberg) is slow (30 minutes for imagine pushing, 800 MiB)
- artefacts don't exist ("are not currently supported by GHES" message somewhere)
- agreed to share link to repo with some Forgejo maintainers for investigation (done)
- example images in docs don't help, because most Actions expect Ubuntu latest, changes to ubuntu-latest
- user was surprised that Actions worked after prefixing the "uses" with github URL (was their intuition only)
- wish Codeberg had an Actions runner
- let: create the actions matrix as described in ...
- ask: how long Actions user? ~year ago
- ask: other CI before? Circle CI once or twice, several years ago

```yaml
on: [push]

jobs:
user-job:
on:
- type: lxc
image: ubuntu
- type: lxc
image: debian
runners: 1
- type: docker
image: alpine:3.19
runners: 2
privileged: true
- type: docker
image: fedora:39
volumes:
- /path/in/action:/path/on/host
- type: lxc
image: debian
arch: arm64
steps:
- run: exit 0
```
