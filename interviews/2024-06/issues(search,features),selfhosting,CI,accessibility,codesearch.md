**Opening the session:**

- background and motivation:
	- main role: follow interested projects (foot on Codeberg), now some own projects
	- estimate time using Forgejo: 5h/week
	- idea about projects: small
- ask: typical **first action of the day**
	- checks email inbox


**Dashboard/Notifications**

- doesn't use notifications


**Pronouns**

- Ask: Aware of pronouns / respect?
- doesn't care, but now that he knows about it, he selects the option so that others are not unsure
- user opens settings while talking
- no-brainer (immediately found in the settings)


**Issues**

- uses public issue trackers on Codeberg
- commonly searches for issues
- **search is confusing:** search for "curl" doesn't bring up "undercurl", search for "curly" finds "undercurl" issues though
- searches "issue regex" in Forgejo issue tracker to learn more, no results
- uses labels, had **difficulties to find the label menu**, searched in the repo settings
- label dependencies
- ask: which of the **features from the sidebar** do you use?
	- labels, no milestone, assignees (note: missed assign to branch/tag and projects, although they were presented in order)
- let: reference another issue
- copies link
- **confused by automatic text expansion** (marking text to replace turns it into a nested link)
- ask: copies link from browser only?
- yes, tries with "#", okay but not used to this workflow


**Admins**

- hosted and is still hosting various forges including sourcehut and forgejo
- advantages on Sourcehut: **external contribution** (issues and patches)
- ask: other reasons?
- loves the Sourcehut **CI/CD (ssh into** build feature)
- ask: deployment / setup
	- Ansible (still Terraform, but wants to get rid of it for licensing reasons)
	- docker containers from official forgejo
	- prefers archlinux package (to connect to native postgres), noticed that it seems to exist
	- postgresql (in docker)
- ask: big problems?
- one big problem: docker registry
	- inexperienced users regularly pushed images until full
	- would appreciate global (**default) cleanup** rules to not rely on nice users
- sign in coupled to keycloak


**External contributions**

- on self-hosted Sourcehut, external users already sent patches via email
- looks forward to federation
- ask: private issue state could allow others to send emails (requires moderator approval)
- appreciated, sourcehut already supports "submit only" permissions (e.g. for security issues)


**PR / Reviewer**

- clone PR via patch download | git am
- knows there is instructions in the GUI
- finds them in the footer section


**Accessibility**

- semi-blind friend on own instance
- **email title should differentiate issues or PRs**


**Code Search**

- fold menu: user clicked and expected to view file, instead the file collapsed
- typo doesn't bring results in fuzzy search
- expcted that match is highlighted
- **multiple matches per file** are "merged", only visible in the code lines (no visual separation)


**New feature compilation**

- let: simultaneous issue comment edit
	- simultaneous edit is possible
	- only error message after saving, no indication during editing
- let: edit a code file in new editor (the new tab behaviour etc) https://codeberg.org/forgejo/forgejo/pulls/4072
- for some reason, the user was on a specific commit instead of a branch (also happened to me from time to time, is there a bug?)
- commit placeholder preview or example
- commit directly is possible to select, although it is prohibited **(branch protection)**
- error message only visible after selecting


**Further notes**

Bug: adding collaborator search box reads "search teams" instead of "search contributors": https://codeberg.org/forgejo/forgejo/issues/4256
