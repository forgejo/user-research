**Opening the session:**

- main role: developer, hopefully maintainer (code reviews) in the future (depends on contributions to their project)
- own instance and Codeberg
	- public projects on Codeberg
	- private projects self-hosted, mirror, configs
- uses Forgejo actions


**Dashboard**

- from dashboard -> usually jumps into repos
- appreciates heatmap


**Organization**

- do: create new org
	- done from dashboard sidebar
- an error message occured (name already used)
	- **radio button about visibility is now disabled** (note: I tried but couldn't reproduce the problem)
- do: create repo that is only used for discussions
- looks for "disable code" feature in new repo form
- reviewer hints: only in repo settings
	- user scrolls a little, finds the setting
	- confused about the **many "save settings" buttons**
	- explains they already pressed the wrong one in the past
- do: give people access to repo via organization
	- looks for button in members, then settings, checks through settings tabs
	- wonders if they might lack the permission
	- creates new team, finally finds the button


**Notifications**

- no unreaad, no pinned
- not a lot of notifications yet
- email notifactions: yes
- gitnext user


**Navbar tabs**

- already used pull requests global tab
- issues tab useful for looking for assigned
- no possibility for label search
- user expects **search syntax** and tries "label: ", "labels: " (no results)
- interviewer tells them that search syntax is not supported and cancels the experiments
- do: return to their previous action (PR)
	- they are still in the Pulls global tab, click on closed, navigate there
	- codeberg is slow for a moment
- ask: from the history, it is visible that the pull was inadvertently closed and then reopened. Why?
	- user tries to remember, there was a need for a rebase, probably wanted to merge and inadvertently closed the pull (not sure exactly, was some time ago already)


**Organization Part Two**

- do: add another member
- visits org via dashboard
- goes to teams, owners
- takes a straightforward route


**Other**

- comments about code reviews: tells about difftastic command line tool
- would be cool if this was somehow integrated
- interacts with language server for advanced information about the changes
- even in unified view, there wouldn't be both red and green lines, but a "changed" line that highlights small added parts in green and red only
- generally, all expected functionality is there
