**Opening the session:**

- background and motivation:
	- was Gitea user for a while (self-hosted)
	- main role: backend developer, co-maintainer of an email client, many other (smaller) projects
	- time using
		- Forgejo: none
		- GitHub: a lot of discovery / distractions
		- Sourcehut: not much, rather productive and focused


**Dashboard**

- do: perform first daily action (not on Codeberg, so only talking)
	- open repo and start coding
	- if not clear what to do: clicking through codebases or issues


**Explore**

- do: explore view
- user sees Forgejo and clicks
- visit issue tracker, filters for label "bug confirmed"
- after reading one issue, they go back to global explore
- they try "language:go" search input
- search syntax not supported, no results


**Organization**

- do: create new organization
	- auto generated avatar is fancy
	- how do they do this? ([x] navbar plus [ ] dashboard sidebar plus)
		- because of the explore view, could be different if done from dashboard
	- which fields? -> straightforward
- do: create new repo for discussions with only issue tracker enabled
	- how: navbar plus
	- notes on new repo dialog:
		- thinking about default label set, decides to create a custom one
		- otherwise straightforward
	- notes on repo settings:
		- no discussion feature
		- hint by interviewer: disable everything but issues
		- user laughs about disabling code on a code hosting platform
- do: add two contributors to the org
	- explain: direct adding to repo vs org
	- they add one via repo (worked fine), one via org
	- is in org settings, confused
	- as ex-Gitea user recalls that you have to go to the teams tab ("I searched for it regularly")
	- user suggests to moev this to settings
	- user recommends **consistency**:
		- (repo settings -> collaborators) and (org settings -> something similar)
		- esp. since there is already blocked users setting


**Notifications**

- note: number: 0
- ask: notifications via email?
	- yes, preferred via email (a miaintainer of an email client in the end :D)
	- not a lot of noise
- duplicate notifications when visiting the other site: mark them all as unread there


**Navbar tabs**

- do: visit issues tab
- suggestion: **pinning some issues** to track current work


**Organization Part Two**

skipped, because user seems to be rather familiar with the concept


**Packages**

- confusion about repo-wide / org wide packages
- redict, for example, shows no package from the repo, but from the org
