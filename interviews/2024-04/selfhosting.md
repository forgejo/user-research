**Opening the session:**

- background and motivation:
	- Gitea user (self-hoster)
- had problems with migrating from Gitea to Forgejo on raspberry
	- Arch repos are outdated
	- didn't find docs on how to build Forgejo from source (which go command to run)
	- main role: software developer and manager, maintainer of a tutorial
- after getting forgejo running:
	- user annoyed about forgejo **not starting without write permissions** to app.ini
	- reason: saving config file as root
	- user recommends: write things like keys etc into a working folder
	- when trying to push:
	~~~
	remote: 2024/04/09 09:32:29 ...s/setting/setting.go:97:InitCfgProvider() [F] Unable to init config provider from "/etc/gitea/app.ini": failed to load config file "/etc/gitea/app.ini": open /etc/gitea/app.ini: permission denied
	~~~
	- nothing useful in the logs
	- problem found: Git hooks were out of sync after migration to Forgejo, maintenance job to synchronize fixed the problem


**Takeaways**

- building forgejo from source not only relevant to developers, but also admins
- the app.ini should be read-only and interactive configs could go somewhere else
- I regularly discover issues that https://github.com/go-gitea/gitea/issues/18995 "central place for Git hooks" would have prevented, going to refresh this by forwarding to Forgejo
