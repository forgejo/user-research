**Opening the session:**

- background and motivation:
	- main role: frontend / fullstack developer
	- estimate time using Forgejo: none, but reviewing PRs on GitLab, mostly Git in cli
	- idea about projects: day job + small jobby projects
	- heard about Gitea vs Forgejo, prefers Forgejo for being more sympathic
- do: navigate to Codeberg
- not yet a user, creates an account
- takes a while to solve the **captcha**, user curses
- switch to email, opens topmost email, but it's the wrong one
- takes a while until the **email** actually arrives
	- note: the reason is a delay by Codeberg's registration throttling script


**Dashboard**

- never used GH as a "socialmedia", not really someone who uses dashboards
- usually goes to **projects directly** and checks for pull requests
- view of open PRs
- navigates to own profile to see project list
- dislikes GitHub's user cards view, prefers the list (like Forgejo)


**Organization**

- creates org from dashboard sidebar
	- reads options carefully, not sure what to do there, no idea what permissions checkbox does
- new repo from sidebar
- notes on the form:
	- confusion about templates
	- **readme dropdown** has only one option
	- confusion about signature trust model
- repo created, navigates to settings
	- tries to find way to disable wiki, clicks through **tabs in sidebar first**
	- then finds config section on main page
	- user confused about checkboxes (close an issue via a commit ...)
	- user confused about repository projects (meaning of the term)
- tries to create team from repo settings, but this doesn't work (because you have to create one first)
- tries to find org settings
- intuitively finds "New team" menu and creates one
	- confused about general access vs admin access
	- would like to fine-tune permissions per repository instead of per team
	- confused about "access to external wiki /issue tarcker" sections
- searches the team to add to the repo
- user confused about navigation back to org page …


**Notifications**

- usually disables all notifications
- prefers notifications to be fully opt-in


**Navbar tabs**

skipped


**Explore**

- attention drawn by icons only
- user searches for filter, filter by language, but this does not exist
- notices topics / tags by repo, but confused about how to filter for them without clicking
- user gives up after a few moments of trying


**Organization Part Two**

- generally confused about hierarchy (nested tabs) in org teams
- finds dark mode "horrible"
	- needs a moment to find the settings, navigates through sidebar tabs first
	- was probably looking for a "dark/light" visual switch
	- switches a light theme
	- still doesn't like how the tabs look
- disagrees to red / **orange tone as primary colour** in forgejo
- suggests to have "undo" after dangerous action instead of confirmation
- cannot **delete org** because there are still repos left (completely prevented)
