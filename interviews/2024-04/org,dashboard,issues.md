**Opening the session:**

- background and motivation:
	- main role: software developer
	- self-hosting forgejo, upgraded to latest version
	- estimate time using Forgejo: about one hour per day, sometimes more
	- idea about projects: many small projects, a few contributions to larger projects
- ask: last thing they did using Forgejo: pushed to a repo


**Dashboard / Issues Page**

- do: perform first daily action
	- visits issues page via navbar
	- not much feedback for now
- do: open dashboard
- ask: expectations, missing items
	- user explains: not interested in "suggestions" like from other platforms ("Hi, do you want to do x now?")
- annoyed by:
	- commit signing not displyed on dashboard
	- heatmap: week starts on Sunday
	- repo search: no results - ah repo is archived
	- reviewer: suggests the filter button next to the search input
	- remark: icon is hard to understand, filter icons are typically different, inconsistent with other "Filter" menues in Forgejo
	- paginadting is only one number in the middle, **would love to skip pages easier**
	- dashboard overall really clean
- user switches to codeberg
	- **issues page lists zero issues**
	- although the user created issues alerady
	- user has at least five issues in forgejo/forgejo created
- Later:
	- dashboard sidebar filter not persistent (archived vs not)
	- confusion about number mismatch in title and tabs
	- confusion about checkbox state (on/off/both)


**Organization**

- do: create new organization
	- how do they do this? ([ ] navbar plus [x] dashboard sidebar plus)
	- **confused about permission checkbox**, reads a few times, tries to understand, asks reviewer
	- tried to use umlaut ("ö") in name, which failed
- do: create new repo for discussions with only issue tracker enabled
	- how do they create the repo? org dashboard sidebar
	- notes on new repo dialog: scans all, skips to the end
	- notes on repo settings: a lot of scrolling to get all the settings
- do: add two contributors to the org
	- creates new team, because of security (not to owners team)
	- scanning all the settings
	- need to click all radio buttons, no "toggle all"
	- considers units carefully, **description in permission table seems to help**
	- confused about "manage actions", we might want to elaborate here
	- confused about checkboxes below table
	- confused about per team settings instead of per repo


**Notifications**

- note: number of unread: 0 | read on Codeberg: 7
- works with notifications
- not often due to single-user instance, and often clicking them away because they are working live with others
- ask: notifications via email? yes
	- prefers in-app UI over email
	- seems to talk about android, too (GitNext?)
	- appreciates that email clients (Thunderbird, K9) group emails that belong together (to one issue, for example)
	- likes notifications for releases


**Navbar tabs**

- user works with issue page regularly
- aim: reduce number of open issues
- picks one issue to work with
- ask: how do you priorize your issues
- size / scope, then motivation for the issue
- uses repo filter most often
- sort and search not often used
- assigned / created etc irrelevant, because self-hosted (single user)
- ask: do you wish for more filter possibilities
- label sorting would be possible, user is undecided if this is appreciated
- ask: do you use deadlines? sometimes yes
- user confused about **next due date in closed issues**, first is without due date, entries with due date are later, then again entries without due date
- confused about **sidebar not showing all repos**
- or maybe not archived ones?
- user is having trouble finding issues
- probably due to **archived repo**
- user shows archived repos with due dates and time tracking


**Organization Part Two**

- do: add another contributor to org
	- uses browser history to get back to the page
- confused about not being able to delete organization with remaining repository
- user rants
- visiting settings from org dashboard not possible
- confused about copying spaces when deleting repo


**Other Notes**

- ask: User explained they have autism, specific challenges?
	- has "logical" ideas and they sometimes differ from normal users
	- mostly having problems with social interactions
	- sometimes behaviour in the software is not logical / consistent
- appreciates that Forgejo uses Forgejo for their development
