- context:
	- 3d printing startup and process monitoring
	- three developers, but growing
	- used Gitea in private before
- usages:
	- using Actions
	- mirroring code from external sources
	- tried using milestones, but now uses external project management
	- didn't get to manage cross-repo work packages
	- code is separated in many small repos
- screenshare
	- switches dashboard content, visits repos from there
	- sync PRs automated
	- needed to modify actions pipeline for custom SSH port
	- uses required approvals on PRs
- uses matrix
- code coverage report
	- llvm coverage test + report to html
	- html is result is attached to comment
	- pandoc formats to markdown to post it
	- demo:
	  ![comment in issue with a small table which holds some coverage information](./2024-08-14_startup,openproject,managed-hosting_coverage.png)
- pull request states (ready to review, review requested etc) for pipeline results https://gitea.com/gitea/act_runner/issues/489
- OpenProject 8.0 integrates with Forgejo, using OP#x references #x in OpenProject
	- they would appreciate if labels could get assigned per workpackage
	- openproject does meta work (global view), Forgejo issues are still used for actionable items for developers
- using managed hosting provider for their services
	- interested in hosted / managed Forgejo, their current offer would be around € 70 / month
