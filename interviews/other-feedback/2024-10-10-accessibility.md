- Codeberg user
- NVDA screenreader on Microsoft Windows
- constant issue:
	- on page refresh, dom is set to the bottom (focusing license picker)
	- **Note:** we managed to partially reproduce the issue, but not yet fully
- "confirm password" prompt confusing ("did I not select the right thing")

---

- switch dashboard context comboox confusing
	- expects navigtation, but nothing happens
	- no dropdown combobox with single entry only 
	- "switch dashboard context" is not exposed properly

---

- help icon in new migration screen has no accessible text
- labels e.g. in new migration screen do not work

---

- general remark: landmarks are helpful
- **repo settings: add button is inside heading**, which is confusing
- confirmed: grouping with fieldsets is helpful (webhook settings)
- confirmed: help text included in issue labels is good, although it adds to the verbosity level

---

- combobox that search while typing is weird (templates in new repository screen), (Note: it behaves weird when using a mouse, too)
- leaving combobox sets focus to top of page? needs confirmation
- combobox traps
	- combobox repo filter on dashboard opens automatically and traps focus
	- would like to have more control over expanding the combobox
